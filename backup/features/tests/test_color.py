"""Test color features.
"""

import time
import unittest

from owl.data import img_tools
from owl.features import common
from owl.features.color import color_cluster
from owl.features.color import color_hist


class ColorFeatTester(unittest.TestCase):
  def test_color_cluster(self):
    color_params = color_cluster.ColorParams()
    color_feat = color_cluster.ColorClusters(color_params)
    img = img_tools.read_img_arr("../../assets/bird.jpg")
    startt = time.time()
    feat = color_feat.compute(img, None)
    print feat
    print "color cluster time: {}s".format(time.time() - startt)
    color_feat.vis_clusters()

  def test_color_hist(self):
    color_params = color_hist.ColorHistParams()
    color_params.color_space = common.ColorSpace.HSV
    color_params.bin_nums = [24, 8, 8]
    color_params.feat_type = common.FeatType.COLOR_HIST_LINEAR
    color_feat = color_hist.ColorHistogram(color_params)
    img = img_tools.read_img_arr("../../assets/bird.jpg")
    startt = time.time()
    feat = color_feat.compute(img, None)
    print feat
    print feat.shape
    print "color hist time: {}s".format(time.time() - startt)


if __name__ == "__main__":
  unittest.main()
