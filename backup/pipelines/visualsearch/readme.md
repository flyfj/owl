Visual search evaluation pipeline that takes images in and perform:

- split images into database and query set
- extract features
- do matching
- compute evaluation metrics and visualize results