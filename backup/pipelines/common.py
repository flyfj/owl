"""Shared data structure of pipelines.
"""


class PipelineMode(object):
  """Mode for pipeline.
  """
  DEVELOPMENT = 0
  DEPLOYMENT = 1
